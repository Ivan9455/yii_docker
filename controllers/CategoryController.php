<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use yii\web\Controller;
use yii\data\Pagination;

class CategoryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex() : string
    {
        return $this->render('index');
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionView(int $id) : string
    {
        if ($category = Category::findOne($id)) {
            $result = $category->getPosts();
            $pages = new Pagination([
                'totalCount' => $result->count(),
                'pageSize' => Yii::$app->params['numberOfPage']
            ]);
            $posts = $result->offset($pages->offset)
                ->limit($pages->limit)->orderBy(['created_at' => SORT_DESC])
                ->all();
            if (count($posts) === 0) {
                return $this->render('/error');
            }

            return $this->render('view', ['pages' => $pages, 'posts' => $posts]);
        }

        return $this->render('/error', ['message' => 'category not found']);
    }
}
