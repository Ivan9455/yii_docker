<?php

namespace app\controllers;

use app\models\Comment;
use app\models\form\CommentForm;
use Yii;
use yii\web\Controller;

class CommentController extends Controller
{
    public function actionSave()
    {
        $model = new CommentForm();
        if (!Yii::$app->user->isGuest && $model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->id = Yii::$app->request->post('CommentForm')['id'];
            $model->user_id = Yii::$app->user->getId();
            if ($model->id) {
                $model->update();

                return $this->render('/post/comment', ['model' => $model, 'comment' => Comment::findOne($model->id)]);
            } else {
                $model->save();
            }
        }

        return $this->redirect(sprintf('/post/%s', $model->post_id));
    }

    public function actionEdit()
    {
        if (Yii::$app->request->post()) {
            $comment = Comment::findOne(Yii::$app->request->post()['id']);
            $model = new CommentForm();
            $model->setAttributes($comment->getAttributes(), false);

            return $this->renderAjax('/comment/update', ['model' => $model, 'post' => $comment->getPost()]);
        }

        return $this->render('/error', ['message' => 'page not found']);
    }

    public function actionUpdate()
    {
        $model = new CommentForm();
        if (!Yii::$app->user->isGuest && $model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->id = Yii::$app->request->post('CommentForm')['id'];
            $model->user_id = Yii::$app->user->getId();
            $model->updateAjax();

            return $this->renderAjax('/post/comment', ['model' => $model, 'comment' => Comment::findOne($model->id)]);
        }

        return false;
    }

    public function actionGet()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->post()) {
            $comment = Comment::findOne(Yii::$app->request->post()['id']);
            $model = new CommentForm();
            $model->setAttributes($comment->getAttributes(), false);

            return $this->renderAjax('/post/comment', ['model' => $model, 'comment' => $comment]);
        }

        return $this->render('/error', ['message' => 'page not found']);
    }

    public function actionRemove()
    {
        $comment = Comment::findOne(Yii::$app->request->post()['id']);
        if (!Yii::$app->user->isGuest && Yii::$app->request->post() && $comment->user_id === Yii::$app->user->getId()) {
            Comment::findOne(Yii::$app->request->post()['id'])->delete();
        }
    }
}
