<?php

namespace app\controllers;

use app\models\Post;
use app\models\Rating;
use Yii;
use yii\web\Controller;

class RatingController extends Controller
{
    public function actionSave()
    {
        $result = Yii::$app->request->post();
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax &&
            ($result['status'] == Rating::TYPE_LIKE || $result['status'] == Rating::TYPE_DISLIKE) &&
            Post::find()->where(['id' => $result['post_id']])->exists()) {
            if ($rating = Rating::findOne(['post_id' => $result['post_id'], 'user_id' => Yii::$app->user->getId()])) {
                if ($rating->status == $result['status']) {
                    $rating->delete();
                } else {
                    $rating->status = $result['status'];
                    $rating->update();
                }
            } else {
                $rating = new Rating();
                $rating->setAttributes([
                    'user_id' => Yii::$app->user->getId(),
                    'post_id' => $result['post_id'],
                    'status' => $result['status']
                ], false);
                $rating->save();
            }
        } else {
            return $this->redirect('/user/login');
        }
    }
}