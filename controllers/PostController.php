<?php

namespace app\controllers;

use app\models\form\CommentForm;
use Yii;
use app\models\form\PostForm;
use app\models\Post;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\FileHelper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use app\models\Comment;

class PostController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex() : string
    {
        $posts = Post::getRecent();
        $pages = new Pagination(['totalCount' => $posts->count(), 'pageSize' => Yii::$app->params['numberOfPage']]);
        $posts = $posts->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->render('index', ['pages' => $pages, 'posts' => $posts,]);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView(int $id) : string
    {
        $post = Post::findOne(['id' => $id]);
        if (empty($post)) {
            throw new NotFoundHttpException();
        } elseif ($post->status_check == Post::STATUS_BAN) {
            return $this->render('/error', ['message' => 'post ban admin']);
        }
        $model = new CommentForm();
        $dataProvider = new ActiveDataProvider([
            'query' => Comment::find()->where(['post_id' => $post->id]),
            'pagination' => [
                'pageSize' => Yii::$app->params['numberOfComment'],
            ],
        ]);

        return $this->render('page', ['post' => $post, 'model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionSave()
    {
        if (!Yii::$app->user->isGuest) {
            $model = new PostForm();
            $model->addSetting('image', ['skipOnEmpty' => false]);
            if ($model->load(Yii::$app->request->post())) {
                $model->save();

                $this->redirect(sprintf('/user/%s', Yii::$app->user->getId()));
            }

            return $this->render('create', ['model' => $model]);
        }

        return $this->redirect('/user/login');
    }

    /**
     * @return string
     */
    public function actionRecent() : string
    {
        return $this->actionIndex();
    }

    /**
     * @return string
     */
    public function actionPopular() : string
    {
        $posts = Post::getPopular();
        $pages = new Pagination(['totalCount' => $posts->count(), 'pageSize' => Yii::$app->params['numberOfPage']]);
        $posts = $posts->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['count_like' => SORT_DESC])
            ->all();

        return $this->render('index', ['pages' => $pages, 'posts' => $posts]);
    }

    /**
     * @param int $post_id
     *
     * @return string|Response
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionEdit(int $post_id)
    {
        $post = Post::findOne($post_id);
        if (!Yii::$app->user->isGuest && $post->user_id === Yii::$app->user->getId()) {
            $model = new PostForm();
            $model->attributes = $post->getAttributes();
            if ($model->load(Yii::$app->request->post())) {
                $model->update($post);
            }

            return $this->render('create', [
                'model' => $model
            ]);
        }

        throw new NotFoundHttpException('post not found or does not belong to this user ');
    }

    /**
     * @param int $post_id
     *
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRemove(int $post_id) : Response
    {
        $post = Post::findOne($post_id);
        if (!Yii::$app->user->isGuest && $post->user_id === Yii::$app->user->getId()) {
            FileHelper::unlink(sprintf('image_db/%s', $post->image));
            $post->delete();
        }

        return $this->redirect('/user/' . Yii::$app->user->getId());
    }

}
