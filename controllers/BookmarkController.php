<?php

namespace app\controllers;

use app\models\Bookmark;
use Yii;
use yii\web\Controller;
use app\models\Post;

class BookmarkController extends Controller
{
    public function actionSave(){
        $result = Yii::$app->request->post();
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax&&
            Post::find()->where(['id'=>$result['post_id']])->exists()) {
            $data= [
                'post_id'=>$result['post_id'],
                'user_id'=>Yii::$app->user->getId()
            ];
            $bookmark = Bookmark::find()->where($data);
            if($bookmark->exists()){
                $bookmark->one()->delete();
            }else{
                $bookmark = new Bookmark();
                $bookmark->setAttributes($data,false);
                $bookmark->save();
            }
        } else {
            return $this->redirect('/user/login');
        }
    }
}
