<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\search\PostSearch;
use yii\data\Pagination;
use yii\web\Response;

class SearchController extends Controller
{
    /**
     * @return string|Response
     */
    public function actionIndex()
    {
        if (Yii::$app->request->get()) {
            $searchModel = new PostSearch();
            $result = $searchModel->search(Yii::$app->request->get()['search']);
            if ($result->count() == 0) {
                return $this->render('/error', ['message' => 'for this request, nothing is not found']);
            }
            $pages = new Pagination([
                'totalCount' => $result->count(),
                'pageSize' => Yii::$app->params['numberOfPage']
            ]);
            $posts = $result->offset($pages->offset)
                ->limit($pages->limit)->orderBy(['created_at' => SORT_DESC])
                ->all();


            return $this->render('/search/index', ['pages' => $pages, 'posts' => $posts]);
        }

        return $this->goBack();
    }
}
