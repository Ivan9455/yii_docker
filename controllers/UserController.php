<?php

namespace app\controllers;

use app\models\form\SignupForm;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\form\LoginForm;

class UserController extends Controller
{
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() : Response
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionSignUp() : string
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->signup() == true) {
                Yii::$app->session->setFlash('success', 'Пользователь успешно зарегистрирован');
            } else {
                Yii::$app->session->setFlash('error', 'Пользователь не зарегистрирван');
            }
        }

        return $this->render('signup', ['model' => $model]);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionView(int $id) : string
    {
        $user = User::findOne(['id' => $id]);
        if ($user && $user->status != User::STATUS_BAN) {
            $result = User::getPosts($user, Yii::$app->params['numberOfPage']);
            if (count($result['posts']) === 0) {
                return $this->render('/error', ['message' => 'post user not fond']);
            }
            $result['setting'] = (Yii::$app->user->getId() == $user->getId()) ? true : false;

            return $this->render('page', $result);
        }

        return $this->render('/error', ['message' => ' user not fond']);
    }

    /**
     * @return string|Response
     */
    public function actionBookmark()
    {
        if (!Yii::$app->user->isGuest) {
            $result = User::getBookmark(Yii::$app->user->getId(), Yii::$app->params['numberOfPage']);
            if (count($result['posts']) === 0) {
                return $this->render('/error', ['message' => 'bookmark not found']);
            }

            return $this->render('page', $result);
        }

        return $this->redirect('/user/login');
    }

    /**
     * @return string
     */
    public function actionBest() : string
    {
        return $this->render('best', ['users' => User::getBest(10)]);
    }
}
