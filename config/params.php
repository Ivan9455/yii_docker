<?php

return [
    'numberOfPage' => 3,
    'numberOfComment' => 5,
    'admin' =>
        [
            'email' => 'admin@example.com',
            'category' => [
                'pageSize' => 10
            ],
            'post' => [
                'pageSize' => 5
            ],
            'comment' => [
                'pageSize' => 10
            ],
            'user' => [
                'pageSize' => 10
            ]
        ]
];
