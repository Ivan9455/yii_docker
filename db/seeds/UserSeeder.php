<?php

use Phinx\Seed\AbstractSeed;



class UserSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'username' => 'ivan',
                'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ1',
                'email' => 'waw@gmail.com',
                'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
                'status' => 2,
                'role' => 2
            ],
            [
                'username' => 'ivan2',
                'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ2',
                'email' => 'pade@gmail.com',
                'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
                'status' => 2,
                'role' => 2
            ],
            [
                'username' => 'ivan3',
                'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ3',
                'email' => 'ivan@test.ru',
                'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
                'status' => 2,
                'role' => 1
            ],
            [
                'username' => 'ivan7',
                'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ4',
                'email' => 'ivanwww@test.ru',
                'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
                'status' => 1,
                'role' => 2
            ],
            [
                'username' => 'ivan4',
                'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ5',
                'email' => 'ivan4@test.ru',
                'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
                'status' => 2,
                'role' => 2
            ]

        ];
        $user = $this->table('user');
        $user->insert($data)->save();
    }
}
