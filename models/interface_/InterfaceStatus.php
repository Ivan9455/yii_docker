<?php

namespace app\models\interface_;

interface InterfaceStatus
{
    public const STATUS_NOT_APPROVED = 1;
    public const STATUS_APPROVED = 2;
    public const STATUS_BAN = 3;

    public function getStatus();

    public static function getStatusAll() : array;
}
