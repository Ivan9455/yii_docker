<?php

namespace app\models\interface_;

interface InterfaceUserStatus
{
    public const STATUS_BAN = 1;
    public const STATUS_ACTIVE = 2;

    public function getStatus();

    public static function getStatusAll() : array;
}
