<?php

namespace app\models;

use app\models\interface_\InterfaceUserStatus;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Admin extends ActiveRecord implements IdentityInterface, InterfaceUserStatus
{
    public const ROLE = 1;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'role' => self::ROLE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getStatus()
    {
        return self::getStatusAll()[$this->status];
    }

    public static function getStatusAll() : array
    {
        return [
            self::STATUS_ACTIVE => "активный пользователь",
            self::STATUS_BAN => "забанен"
        ];
    }
}
