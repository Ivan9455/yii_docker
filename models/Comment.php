<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\interface_\InterfaceStatus;

class Comment extends ActiveRecord implements InterfaceStatus
{
    /**
     * @return User|null
     */
    public function getUser() : ?User
    {
        return User::findOne(['id' => $this->user_id]);
    }

    /**
     * @return Post|null
     */
    public function getPost() : ?Post
    {
        return Post::findOne(['id' => $this->post_id]);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return self::getStatusAll()[$this->status_check];
    }

    /**
     * @return array
     */
    public static function getStatusAll() : array
    {
        return [
            self::STATUS_NOT_APPROVED => 'Не проверенно',
            self::STATUS_APPROVED => 'Проверенно',
            self::STATUS_BAN => 'Забанен'
        ];
    }
}
