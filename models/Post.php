<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\interface_\InterfaceStatus;

class Post extends ActiveRecord implements InterfaceStatus
{
    public const CHAR_PER_MINUTE = 1200;
    public const COMMENT_ON = 1;
    public const COMMENT_OFF = 2;
    /**
     * @return int
     */
    public function getCountLike() : int
    {
        return Rating::find()->where(['post_id' => $this->id, 'status' => Rating::TYPE_LIKE])->count();
    }

    /**
     * @return int
     */
    public function getCountDislike() : int
    {
        return Rating::find()->where(['post_id' => $this->id, 'status' => Rating::TYPE_DISLIKE])->count();
    }

    /**
     * @return array of Post object
     */
    public function getComments() : array
    {
        return Comment::find()->where(['post_id' => $this->id])->all();
    }

    /**
     * @return Category
     */
    public function getCategory() : Category
    {
        return Category::findOne(['id' => $this->category_id]);
    }

    /**
     * @return User
     */
    public function getUser() : User
    {
        return User::findOne(['id' => $this->user_id]);
    }

    /**
     * @return bool
     */
    public function isLike() : bool
    {
        $rating = $this->getRating();
        if ($rating !== null) {
            return $rating->status == Rating::TYPE_LIKE;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isDisLike() : bool
    {
        $rating = $this->getRating();
        if ($rating !== null) {
            return $rating->status == Rating::TYPE_DISLIKE;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isBookmark() : bool
    {
        return !empty(Bookmark::findOne([
            'user_id' => Yii::$app->getUser()->id,
            'post_id' => $this->id
        ]));
    }

    /**
     * @return Rating|null
     */
    protected function getRating() : ?Rating
    {
        $rating = Rating::findOne([
            'user_id' => Yii::$app->getUser()->id,
            'post_id' => $this->id
        ]);

        return $rating;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getRecent()
    {
        return Post::find()->where(['<>', 'status_check', Post::STATUS_BAN]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getPopular()
    {
        return Post::find()->select([
            sprintf('
            * , COALESCE((SELECT COUNT(rating.status)
             FROM rating WHERE rating.status = %s
            AND post.id = rating.post_id)) as count_like', Rating::TYPE_LIKE)
        ])->where(['<>', 'status_check', Post::STATUS_BAN]);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return self::getStatusAll()[$this->status_check];
    }

    /**
     * @return array
     */
    public static function getStatusAll() : array
    {
        return [
            self::STATUS_NOT_APPROVED => 'Не проверенно',
            self::STATUS_APPROVED => 'Проверенно',
            self::STATUS_BAN => 'Забанен'
        ];
    }
}
