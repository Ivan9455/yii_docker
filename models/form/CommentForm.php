<?php

namespace app\models\form;

use app\models\Comment;
use app\models\Post;
use Yii;
use yii\base\Model;

class CommentForm extends Model
{
    public $id;
    public $post_id;
    public $user_id;
    public $content;

    public function rules()
    {
        return [
            [['content'], 'required'],
            ['content', 'string', 'max' => 1024],
            ['post_id', 'checkPost']
        ];
    }

    public function checkPost()
    {
        if (is_null(Post::findOne(['id' => $this->post_id]))) {
            $this->addError('post_id', 'Данного поста не существует');
        }
    }

    public function save()
    {
        $comment = new Comment();
        $comment->setAttributes($this->getAttributes(), false);
        $comment->save();
    }

    public function updateAjax()
    {
        $comment = Comment::findOne($this->id);
        $comment->setAttributes($this->getAttributes(), false);
        $comment->update();
    }

}
