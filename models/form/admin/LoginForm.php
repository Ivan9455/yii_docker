<?php

namespace app\models\form\admin;

use app\models\Admin;
use app\models\form\LoginForm as Form;

class LoginForm extends Form
{
    /**
     * @return int
     */
    protected function getRole() : int
    {
        return Admin::ROLE;
    }
}
