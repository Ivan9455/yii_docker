<?php

namespace app\models\form;

use app\models\Category;
use Yii;
use yii\base\Model;
use app\models\Post;
use yii\helpers\FileHelper;
use \yii\helpers\Html;
use yii\web\UploadedFile;

class PostForm extends Model
{
    public $id;
    public $user_id;
    public $category_id;
    public $image;
    public $title;
    public $content;
    public $time_read;
    public $is_comment;
    protected $setting = [
        'image' => ['skipOnEmpty' => true]
    ];

    /**
     * @return array
     */
    public function getSetting() : array
    {
        return $this->setting;
    }

    /**
     * @param array $setting
     */
    public function setSetting(array $setting) : void
    {
        $this->setting = $setting;
    }

    /**
     * @param string $pole
     * @param array $rules
     */
    public function addSetting(string $pole, array $rules) : void
    {
        $this->setting[$pole] = $rules;
    }

    public function rules() : array
    {
        return [
            [['category_id', 'title', 'content'], 'required'],
            ['title', 'string', 'max' => 255],
            ['category_id', 'checkCategory'],
            ['content', 'string'],
            ['is_comment','string'],
            [
                'image',
                'image',
                'skipOnEmpty' => $this->setting['image']['skipOnEmpty'],
                'extensions' => 'png, jpg, jpeg',
                'minWidth' => 200,
                'maxWidth' => 750,
                'minHeight' => 200,
                'maxHeight' => 750,
                'maxSize' => 1024 * 1024 * 2
            ]
        ];
    }

    /**
     * @return Post|null
     * @throws \yii\base\Exception
     */
    public function save()
    {

        $this->image = UploadedFile::getInstance($this, 'image');
        $fileName = sprintf('%s.%s', Yii::$app->getSecurity()->generateRandomString(),
            $this->image->extension);
        $this->image->saveAs("image_db/{$fileName}");
        $this->image = $fileName;
        $this->user_id = Yii::$app->user->getId();
        $this->title = Html::encode($this->title);
        $this->content = Html::encode($this->content);
        $this->time_read = sprintf("%s min", ceil(strlen($this->content) / Post::CHAR_PER_MINUTE));
        $this->is_comment = ($this->is_comment==0)?Post::COMMENT_OFF:Post::COMMENT_ON;
        $post = new Post();
        $post->setAttributes($this->getAttributes(), false);

        return $post->save() ? $post : null;
    }

    public function checkCategory() : void
    {
        if (is_null(Category::findOne(['id' => $this->category_id]))) {
            $this->addError('category_id', 'Данной категории не существует');
        }
    }

    /**
     * @param Post $post
     *
     * @return Post|null
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function update(Post $post) : ?Post
    {
        $post->title = Html::encode($this->title);
        $post->content = Html::encode($this->content);
        $post->time_read = sprintf("%s min", ceil(strlen($this->content) / Post::CHAR_PER_MINUTE));
        $post->is_comment = ($this->is_comment==0)?Post::COMMENT_OFF:Post::COMMENT_ON;
        if (UploadedFile::getInstance($this, 'image')) {
            $post->image = UploadedFile::getInstance($this, 'image');
            $fileName = sprintf('%s.%s', Yii::$app->getSecurity()->generateRandomString(),
                $this->image->extension);
            $post->image->saveAs("image_db/{$fileName}");
            $post->image = $fileName;
            FileHelper::unlink(sprintf('image_db/%s', Post::findOne($post->id)->image));
        }

        return $post->update() ? $post : null;
    }

}
