<?php

namespace app\models\search;

use app\models\Post;
use yii\db\ActiveQuery;
use yii\helpers\Html;

class PostSearch extends Post
{
    /**
     * @param string $search
     *
     * @return \yii\db\ActiveQuery
     */
    public function search(string $search) : ActiveQuery
    {
        $search = Html::encode($search);

        return Post::find()
            ->orWhere(sprintf("user_id in (select user.id from user where user.username like '%s%%')", $search))
            ->orWhere(sprintf("(category_id in (select category.id from category 
            where category.title like '%s%%' or category.description like '%s%%') )", $search, $search))
            ->orFilterWhere([
                'like',
                'title',
                sprintf('%s%s%s', '%', $search, '%'),
                false
            ])
            ->orFilterWhere([
                'like',
                'content',
                sprintf('%s%s%s', '%', $search, '%'),
                false
            ]);
    }
}
