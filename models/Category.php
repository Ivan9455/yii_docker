<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Category extends ActiveRecord
{
    /**
     * @return Category|null
     */
    public function getParent() : ?self
    {
        return Category::findOne(['id' => $this->parent_id]);
    }

    /**
     * @return array
     */
    public function getBreadcrumbs() : array
    {
        $result[] = $this;
        $parent = $this->getParent();
        if ($parent !== null) {
            $result = array_merge($result, $parent->getBreadcrumbs());
        }

        return $result;
    }

    /**
     * @return array
     */
    public static function getDropDownList() : array
    {
        $result = [];
        $categories = Category::find()->where(['parent_id' => null])->all();
        foreach ($categories as $category) {
            $result[] = [
                'id' => $category->id,
                'title' => $category->title
            ];
            $children = $category->getChildrenDropDownList();
            for ($i = 0; $i < count($children); $i++) {
                $children[$i]['title'] = '--' . $children[$i]['title'];
                array_push($result, $children[$i]);
            }
        }

        return ArrayHelper::map($result, 'id', 'title');
    }

    /**
     * @return array
     */
    protected function getChildrenDropDownList() : array
    {
        $result = [];
        $categories = Category::find()->where(['parent_id' => $this->id])->all();
        foreach ($categories as $category) {
            $result[] = [
                'id' => $category->id,
                'title' => $category->title
            ];
            $children = $category->getChildrenDropDownList();
            for ($i = 0; $i < count($children); $i++) {
                $children[$i]['title'] = '--' . $children[$i]['title'];
                array_push($result, $children[$i]);
            }
        }

        return $result;
    }

    /**
     * @return array|Category[]
     */
    public function getChildren() : array
    {
        return Category::find()->where(['parent_id' => $this->id])->all();
    }

    /**
     * @return array|\yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return Post::find()->where(['category_id' => $this->id]);
    }
}
