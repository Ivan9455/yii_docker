<?php

namespace app\models;

use app\models\interface_\InterfaceUserStatus;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\data\Pagination;

class User extends ActiveRecord implements IdentityInterface, InterfaceUserStatus
{
    public const ROLE = 2;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return static|null
     */
    public static function findByUsername(string $username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken(string $token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param $password
     * Generates password hash from password and sets it to the model
     *
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param int $limit
     *
     * @return array|ActiveRecord[]
     */
    public static function getBest(int $limit) : array
    {
        $users = User::find()->select([
            '
            * , COALESCE((
  SELECT COUNT(rating.status) 
  FROM rating , post WHERE rating.status = 1 
  AND rating.post_id = post.id AND post.user_id = user.id
  )) AS count_like
            '
        ])->limit($limit)->orderBy(['count_like' => SORT_DESC])->all();

        return $users;
    }

    /**
     * @return int|string
     */
    public function getRatingLike()
    {
        $posts = ArrayHelper::map(
            Post::find()->select('id')->where(['user_id' => $this->id])->all(), 'id', 'id');

        return Rating::find()->where(['post_id' => $posts, 'status' => Rating::TYPE_LIKE])->count();
    }

    /**
     * @return int|string
     */
    public function getRatingDisLike()
    {
        $posts = ArrayHelper::map(
            Post::find()->select('id')->where(['user_id' => $this->id])->all(), 'id', 'id');

        return Rating::find()->where(['post_id' => $posts, 'status' => Rating::TYPE_DISLIKE])->count();;
    }

    /**
     * @param int|string $user_id
     * @param int $pageSize
     *
     * @return array
     */
    public static function getBookmark($user_id, int $pageSize) : array
    {
        $query = Post::find()->where(sprintf("id in (select bookmark.post_id from bookmark 
        where bookmark.user_id = %s)", $user_id));
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => $pageSize]);
        $posts = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['created_at' => SORT_DESC])
            ->all();

        return ['pages' => $pages, 'posts' => $posts];
    }

    /**
     * @param User $user
     * @param int $pageSize
     *
     * @return array
     */
    public static function getPosts(User $user, int $pageSize) : array
    {
        $query = Post::find()->where(['user_id' => $user->getId()]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => $pageSize]);
        $posts = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['created_at' => SORT_DESC])
            ->all();

        return ['pages' => $pages, 'posts' => $posts];
    }

    public function getStatus()
    {
        return self::getStatusAll()[$this->status];
    }

    public static function getStatusAll() : array
    {
        return [
            self::STATUS_ACTIVE => "активный пользователь",
            self::STATUS_BAN => "забанен"
        ];
    }

    public function getRole()
    {
        return self::getRoleAll()[$this->role];
    }

    public static function getRoleAll() : array
    {
        return [
            1 => 'admin',
            2 => 'user'
        ];
    }
}
