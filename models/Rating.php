<?php

namespace app\models;
use yii\db\ActiveRecord;

class Rating extends ActiveRecord
{
    public const TYPE_LIKE = 1;
    public const TYPE_DISLIKE = 2;

}