<?php
/* @var $post app\models\Post */

use \yii\helpers\HtmlPurifier;

?>
<div class="col-xs-12 border border-primary">
    <div class="row left_menu">
        <h2><?= HtmlPurifier::process($post->title); ?></h2>
        <p>Автор:
            <a href="/user/<?= $post->getUser()->id; ?>"><?= $post->getUser()->username; ?></a>
        </p>
        <div class="row ">
            <div class="col-xs-12">
                <?php
                $array = array_reverse($post->getCategory()->getBreadcrumbs());
                $breadcrumbs = [];
                for ($i = 0; $i < count($array); $i++) {
                    $breadcrumbs[] = "<a href='/category/" . $array[$i]->id . "'>" . $array[$i]->title . "</a>";
                }
                echo implode($breadcrumbs, ' > ');
                ?>
            </div>
            <div class="col-xs-12 padding-x-xs ">Time read :
                <?= $post->time_read ?>
            </div>
            <div class="col-xs-12 padding-x-xs ">
                <?= mb_strimwidth(HtmlPurifier::process($post->content),0,150,"..."); ?>
            </div>
            <div class="col-xs-12 padding-x-xs "><br>
                <?= $post->created_at; ?>
            </div>
            <br>
        </div>
        <div class="status" data-id="<?= $post->id ?>">
            <div class="col-xs-3 col-sm-3 col-md-3 like item">
                <i class="<?= $post->isLike() ? "fas" : "far"; ?> fa-thumbs-up"></i>
                <div class="item_number">
                    <?= $post->getCountLike(); ?>
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 dislike item">
                <i class="<?= $post->isDisLike() ? "fas" : "far"; ?> fa-thumbs-down"></i>
                <div class="item_number">
                    <?= $post->getCountDislike(); ?>
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 bookmark item">
                <i class="<?= $post->isBookmark() ? "fas" : "far"; ?> fa-bookmark"></i>
            </div>
            <div class="col-xs-1 col-sm-3 col-md-3">
                <a href="/post/<?= $post->id; ?>">
                    <i class="fas fa-angle-right"></i>
                </a>
            </div>
        </div>
        <br>
        <div class="col-xs-12 padding-x-xs ">
            Лучший автор :
            <a href="/user/<?= $user->id ?>"><?= $user->username ?></a>
        </div>
    </div>
</div>



