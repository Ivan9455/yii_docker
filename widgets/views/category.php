<div class="list-group">
    <?php
    foreach ($categories as $category) { ?>
        <div><a href="/category/<?= $category->id ?>" class="list-group-item list-group-item-action">
                <h5 class="list-group-item-heading"><?= $category->title ?></h5>
                <p class="list-group-item-text"><?= $category->description ?></p>
            </a><?= $this->render('template/category', ['categories' => $category->getChildren()]) ?></div>
        <?php
    }
    ?>
</div>

