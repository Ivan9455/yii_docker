<?php

namespace app\widgets;

use  yii\bootstrap\Widget;

class SearchWidget extends Widget
{
    /**
     * @return string
     */
    public function run():string
    {
        return $this->render('search');
    }

}