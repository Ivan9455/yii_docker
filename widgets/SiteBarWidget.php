<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 02.04.19
 * Time: 17:04
 */

namespace app\widgets;

use  yii\bootstrap\Widget;
use app\models\User;
use app\models\Post;

class SiteBarWidget extends Widget
{
    public function run() : string
    {
        return $this->render('sitebar', [
            'post' => Post::getPopular()->limit(1)->orderBy(['count_like' => SORT_DESC])->one(),
            'user' => User::getBest(1)[0]
        ]);
    }
}
