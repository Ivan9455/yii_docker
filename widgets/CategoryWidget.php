<?php

namespace app\widgets;

use app\models\Category;
use  yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;

class CategoryWidget extends Widget
{
    public function run()
    {
        return $this->render('category', ['categories' => Category::find()->where(['parent_id' => null])->all()]);
    }

}
