<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use app\widgets\SearchWidget;


AppAsset::register($this);
$this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>


<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            SearchWidget::widget(),
            Yii::$app->user->isGuest ? (
            ['label' => 'Sign up', 'url' => ['/user/sign-up']]) :
                Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/user/login']]
                ) : (
                [
                    'label' => Yii::$app->user->identity->username,
                    'url' => [sprintf('user/%s', Yii::$app->user->identity->getId())],
                    'items' => [
                        ['label' => 'My page', 'url' => [sprintf('/user/%s', Yii::$app->user->identity->getId())]],
                        ['label' => 'Categories', 'url' => ['/categories']],
                        ['label' => 'Post create', 'url' => ['/post/save']],
                        ['label' => 'Bookmark', 'url' => ['/user/bookmark']],
                        ['label' => 'Logout', 'url' => ['/user/logout']],
                    ]
                ]
                )
        ],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
