<aside class="main-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Users', 'icon' => 'users', 'url' => ['/admin/users']],
                    ['label' => 'Posts', 'icon' => 'info', 'url' => ['/admin/posts']],
                    ['label' => 'Categories', 'icon' => 'tags', 'url' => ['/admin/categories']],
                    ['label' => 'Comments', 'icon' => 'comment', 'url' => ['/admin/comments']],
                ],
            ]
        ) ?>
    </section>
</aside>
