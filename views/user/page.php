<?php

use yii\widgets\LinkPager;

/* @var $posts array */
/* @var $pages array */
?>
<div class="row">
    <div class="col-12 col-md-9">
        <?php
        foreach ($posts as $post) {
            echo $this->render('/post/template/get', ['post' => $post, 'setting' => $setting]);
        }
        ?>
        <?= LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
    </div>
</div>