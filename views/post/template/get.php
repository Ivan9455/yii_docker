<?php
/* @var $post app\models\Post */

use \yii\helpers\HtmlPurifier;

?>
<div class="col-xs-12 border border-primary">
    <div class="row">
        <h2><?= HtmlPurifier::process($post->title); ?></h2>
        <p>Автор:
            <a href="/user/<?= $post->getUser()->id; ?>"><?= $post->getUser()->username; ?></a>
        </p>
        <div class="row">
            <div class="col-xs-12">
                <?php
                $array = array_reverse($post->getCategory()->getBreadcrumbs());
                $breadcrumbs = [];
                for ($i = 0; $i < count($array); $i++) {
                    $breadcrumbs[] = "<a href='/category/" . $array[$i]->id . "'>" . $array[$i]->title . "</a>";
                }
                echo implode($breadcrumbs, ' > ');
                ?>
            </div>
            <div class="col-xs-12 padding-x-xs ">Time read :
                <?= $post->time_read ?>
            </div>
            <div class="col-xs-12 padding-x-xs ">
                <img src="/image_db/<?= $post->image; ?>" class="img-responsive pull-right">
                <?php
                if($textMaxLength){
                    echo HtmlPurifier::process($post->content);
                }else{
                    echo mb_strimwidth(HtmlPurifier::process($post->content),0,1024,"...");
                }
                 ?>
            </div>
            <div class="col-xs-12 padding-x-xs "><br>
                <?= $post->created_at; ?>
            </div>
            <br>
        </div>
        <div class="status" data-id="<?= $post->id ?>">
            <div class="col-xs-3 col-sm-2 col-md-2 like item">
                <i class="<?= $post->isLike() ? "fas" : "far"; ?> fa-thumbs-up"></i>
                <div class="item_number">
                    <?= $post->getCountLike(); ?>
                </div>
            </div>
            <div class="col-xs-3 col-sm-2 col-md-2 dislike item">
                <i class="<?= $post->isDisLike() ? "fas" : "far"; ?> fa-thumbs-down"></i>
                <div class="item_number">
                    <?= $post->getCountDislike(); ?>
                </div>
            </div>
            <div class="col-xs-3 col-sm-2 col-md-2 bookmark item">
                <i class="<?= $post->isBookmark() ? "fas" : "far"; ?> fa-bookmark"></i>
            </div>
            <div class="col-xs-1 col-sm-1 col-md-1">
                <a href="/post/<?= $post->id; ?>">
                    <i class="fas fa-angle-right"></i>
                </a>
            </div>
            <?php
            if ($setting) {
                ?>
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <a href="/post/edit/<?= $post->id ?>">
                        edit
                    </a>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <a href="/post/remove/<?= $post->id ?>">
                        remove
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>



