<?php
/* @var $comment \app\models\Comment */

?>

<div class="col-xs-12 alert alert-info ">
    <a href="/user/<?= $comment->user_id ?>"><?= $comment->getUser()->username ?></a><br>
    <?= $comment->content ?>
    <p>
        <?= $comment->created_at ?>
    </p>
    <?php
    if (Yii::$app->user->getId() === $comment->user_id) {
        ?>
        <div data-id="<?= $comment->id ?>">
            <div class="comment remove_comment">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <div class="comment edit_comment">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </div>
        </div>
    <?php } ?>
</div>


