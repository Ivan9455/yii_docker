<?php

use yii\bootstrap\Nav;
use yii\widgets\LinkPager;
use app\widgets\SiteBarWidget;

/* @var $this yii\web\View */
/* @var $post  app\models\Post */
/* @var $posts  array of Post */
/* @var $pages \yii\data\Pagination */

$this->title = 'My Yii Application';

?>
<div class="site-index">
    <div class="row">
        <?= Nav::widget([
            'items' => [
                ['label' => 'Recent', 'url' => ['post/recent'], 'options' => ['class' => 'col-xs-4']],
                ['label' => 'Popular', 'url' => ['post/popular'], 'options' => ['class' => 'col-xs-4']],
                ['label' => 'Author Best', 'url' => ['user/best'], 'options' => ['class' => 'col-xs-4']]
            ]
        ]); ?>
        <div class="col-12 col-md-9">
            <?php
            foreach ($posts as $post) {
                echo $this->render('template/get', ['post' => $post]);
            }
            ?>
            <?= LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div>
        <div class=" visible-md-block visible-lg-block visible-xl-block col-md-3">
            <?= SiteBarWidget::widget() ?>
        </div>
    </div>
</div>
