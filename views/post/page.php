<?php
/* @var $post \app\models\Post */
/* @var $model \app\models\Comment*/
use \app\models\Comment;

use app\assets\CommentAsset;
use yii\widgets\ListView;
use kop\y2sp\ScrollPager;
use \app\models\Post;
CommentAsset::register($this);
echo $this->render('template/get', ['post' => $post, 'textMaxLength' => true]);
?>
<div class="col-xs-12">
    <?php
    if($post->is_comment==Post::COMMENT_ON){
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function (Comment $model, $key, $index, $widget) {
                if($model->status_check==Comment::STATUS_BAN){
                    return '<div class="col-xs-12 alert alert-info ">comment remove</div>';
                }
                return $this->render('comment', ['comment' => $model]);

            },
            'pager' => [
                'class' => ScrollPager::class,
                'enabledExtensions' => [
                    ScrollPager::EXTENSION_TRIGGER,
                    ScrollPager::EXTENSION_SPINNER
                ],
            ],
        ]);
        echo $this->render('/comment/save', ['model' => $model, 'post' => $post]);
    }
    ?>
</div>
<?php



