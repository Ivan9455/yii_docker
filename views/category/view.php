<?php

use yii\bootstrap\Nav;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $post app\models\Post */
/* @var $posts array of Post */
/* @var $pages \yii\data\Pagination */

$this->title = 'post_category';

?>
<div class="category-view">
    <div class="row">
        <div class="col-12 col-md-12">
            <?php
            foreach ($posts as $post) {
                echo $this->render('/post/template/get', ['post' => $post]);
            }
            ?>
            <?= LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div>
    </div>
</div>