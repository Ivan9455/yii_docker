<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $post \app\models\Post */
/* @var $model \app\models\form\CommentForm */

if (!Yii::$app->user->isGuest) {
    if (Yii::$app->session->hasFlash('success')) {
        echo Yii::$app->session->getFlash('success');
    } elseif (Yii::$app->session->hasFlash('error')) {
        echo Yii::$app->session->getFlash('error');
    }
    $form = ActiveForm::begin(['action' => '/comment/update']);
    echo $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false);
    echo $form->field($model, 'post_id')->hiddenInput(['value' => $post->id])->label(false);
    echo $form->field($model, 'content')->textarea(['rows' => '4']); ?>
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary comment_update']) ?>
        <div class="btn btn-primary get_comment" data-id="<?= $model->id ?>">back</div>
    </div>
    <?php
    ActiveForm::end();
}
?>
