<?php

use yii\db\Migration;

/**
 * Class m190308_055712_rating
 */
class m190308_055712_rating extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'user_id' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull(),
            'status' => "ENUM('1','2')"
        ]);
        $this->addForeignKey(
            'fk-rating-user_id',
            'rating',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-rating-post_id',
            'rating',
            'post_id',
            'post',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rating');
    }
}
