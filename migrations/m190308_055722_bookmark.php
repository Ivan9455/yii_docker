<?php

use yii\db\Migration;

/**
 * Class m190308_055722_bookmark
 */
class m190308_055722_bookmark extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bookmark', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'user_id' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'fk-bookmark-user_id',
            'bookmark',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-bookmark-post_id',
            'bookmark',
            'post_id',
            'post',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bookmark');
    }
}
