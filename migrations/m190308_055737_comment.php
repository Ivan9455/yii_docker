<?php

use yii\db\Migration;

/**
 * Class m190308_055737_comment
 */
class m190308_055737_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'user_id' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull(),
            'content'=> $this->string(2048)->notNull(),
            'updated_at' => $this->timestamp(),
            'created_at' => $this->dateTime(),
            'time_checked' => $this->dateTime(),
            'status_check' => "ENUM('1','2','3') NOT NULL DEFAULT '1'"
        ]);
        $this->addForeignKey(
            'fk-comment-user_id',
            'comment',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-comment-post_id',
            'comment',
            'post_id',
            'post',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comment');
    }
}
