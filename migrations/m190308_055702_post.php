<?php

use yii\db\Migration;

/**
 * Class m190308_055702_post
 */
class m190308_055702_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'user_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'image' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text()->notNull(),
            'time_read' => $this->string(255)->notNull(),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->timestamp(),
            'time_checked' => $this->dateTime(),
            'status_check' => "ENUM('1','2','3') NOT NULL DEFAULT '1'",
            'is_comment' => "ENUM('1','2') NOT NULL DEFAULT '1'"
        ]);

        $this->addForeignKey(
            'fk-post-user_id',
            'post',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-post-category_id',
            'post',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
