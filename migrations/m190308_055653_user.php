<?php

use yii\db\Migration;
use app\models\User;
use app\models\Admin;

/**
 * Class m190308_055653_user
 */
class m190308_055653_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'username' => $this->string(32)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'role' => sprintf("ENUM('%s','%s') NOT NULL DEFAULT '%s'",
                Admin::ROLE,
                User::ROLE,
                User::ROLE
            ),
            'status' => sprintf("ENUM('%s','%s') NOT NULL DEFAULT '%s'",
                User::STATUS_BAN,
                User::STATUS_ACTIVE,
                User::STATUS_ACTIVE
            ),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
