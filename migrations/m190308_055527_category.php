<?php

use yii\db\Migration;

/**
 * Class m190308_055527_category
 */
class m190308_055527_category extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        Yii::$app->db->createCommand('ALTER DATABASE `yii_db` COLLATE utf8_general_ci')->execute();
        $this->createTable('category', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'parent_id' => $this->integer(),
            'title' => $this->string(50)->notNull(),
            'description' => $this->string(255)->notNull()
        ]);
        $this->addForeignKey(
            'fk-category-parent_id',
            'category',
            'parent_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
