<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \app\modules\admin\models\form\PostForm */

/* @var $categories array of \app\models\Category */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Category;
use app\models\Post;
$this->title = 'Update';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="post-create">
    <div class="row">
        <div class="col-lg-5">
            <?php
            if (Yii::$app->session->hasFlash('success')) {
                echo Yii::$app->session->getFlash('success');
            } elseif (Yii::$app->session->hasFlash('error')) {
                echo Yii::$app->session->getFlash('error');
            }
            $form = ActiveForm::begin(['id' => 'save-form', 'options' => ['enctype' => 'multipart/form-data']]);
            ?>
            <?= $form->field($model, 'category_id')->label('Category')->dropDownList(
                Category::getDropDownList(),
                $params ? $params : ['prompt' => 'выберете катигорию']); ?>

            <?= $form->field($model, 'image')->fileInput(); ?>
            <?php if ($model->image): ?>
                <img src="/image_db/<?= $model->image ?>" alt="">
            <?php endif; ?>
            <?= $form->field($model, 'title')->input('text'); ?>
            <?= $form->field($model, 'content')->textarea(['rows' => '7']); ?>
            <p>Коментарии</p>
            <?= $form->field($model, 'is_comment')->checkbox()->label('вкл/выкл'); ?>
            <?= $form->field($model, 'status_check')->label('Статус')->dropDownList(
                Post::getStatusAll() ); ?>
            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
