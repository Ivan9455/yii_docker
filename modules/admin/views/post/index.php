<?php

use yii\grid\GridView;
use \app\models\Post;
use yii\helpers\HtmlPurifier;
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'user_id',
            'label' => 'Пользователь',
            'format' => 'text',
            'content' => function (Post $data) {
                return $data->getUser()->username;
            },
        ],
        [
            'attribute' => 'category_id',
            'label' => 'Категория',
            'format' => 'text',
            'content' => function (Post $data) {
                return $data->getCategory()->title;
            },
        ],
        'title:ntext',
        [
            'attribute' => 'content',
            'label'=>'Контент',
            'format'=>'text',
            'content'=>function(Post $post){
                return mb_strimwidth(HtmlPurifier::process($post->content),0,256,"...");
            }
        ],
        [
            'attribute' => 'status_check',
            'label' => 'Категория',
            'format' => 'text',
            'content' => function (Post $data) {
                return $data->getStatus();
            },
            'filter' => Post::getStatusAll()
        ],
        ['class' => 'yii\grid\ActionColumn'],
    ],
]);
?>
