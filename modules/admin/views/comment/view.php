<?php
/* @var $comment \app\models\Comment */
?>
<div class="col-xs-12 alert alert-dark ">
    Ссылка на пост<a href="/post/<?= $comment->post_id ?>" class="btn btn-primary">тык</a><br>
    Автор : <a href="/user/<?= $comment->user_id ?>"
               class="btn btn-primary "><?= $comment->getUser()->username ?></a><br>
    <?= $comment->content ?>
    <p>
        <?= $comment->created_at ?>
    </p>
    <a href="/admin/comments" class="btn btn-primary">Назад</a>
</div>
