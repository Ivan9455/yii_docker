<?php

use yii\grid\GridView;
use \app\models\Comment;
use yii\helpers\Html;
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [
            'attribute'=>'post_id',
            'label' => 'Ссылка на пост',
            'format' => 'text',
            'content'=>function(Comment $data){
                return Html::a('url',sprintf('/post/%s',$data->post_id),['target'=>'_blank']);
            }
        ],
        [
            'attribute' => 'user_id',
            'label' => 'Пользователь',
            'format' => 'text',
            'content' => function (Comment $data) {
                return $data->getUser()->username;
            },
        ],
        [
            'attribute'=>'content',
            'label' => 'Текст коментария',
            'format' => 'text',
            'content'=>function(Comment $data){
                return $data->content;
            }
        ],
        [
            'attribute' => 'status_check',
            'label' => 'Статус',
            'format' => 'text',
            'content' => function(Comment $data){
                return $data->getStatus();
            },
            'filter' => Comment::getStatusAll()
        ],
        ['class' => 'yii\grid\ActionColumn'],
    ],
]);
?>
