<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Comment;
/* @var $model \app\modules\admin\models\form\CommentForm */

if (Yii::$app->session->hasFlash('success')) {
    echo Yii::$app->session->getFlash('success');
} elseif (Yii::$app->session->hasFlash('error')) {
    echo Yii::$app->session->getFlash('error');
}
$form = ActiveForm::begin(); ?>
<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'post_id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'content')->textarea(['rows' => '4']); ?>
<?= $form->field($model, 'status_check')->label('Статус')->dropDownList(
    Comment::getStatusAll()); ?>
<div class="form-group">
    <?= Html::submitButton('Update', ['class' => 'btn btn-primary comment_update']) ?>
    <div class="btn btn-primary get_comment" data-id="<?= $model->id ?>">back</div>
</div>
<?php
ActiveForm::end();
?>
