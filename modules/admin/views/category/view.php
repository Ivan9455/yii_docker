<?php
/**
 * @var $category \app\models\Category
 */

if($category->getParent()){
    ?>
    <p>Parent category : <a href="/admin/category/view?id=<?= $category->parent_id ?>"><?= $category->getParent()->title ?></a></p>
<?php
}
?>
<p>title : <?= $category->title ?></p>
<p>description : <?= $category->description ?></p>
<p>
    <a href="/admin/category/update?id=<?= $category->id ?>">Edit</a>
</p>
<p>
    <a href="/admin/category/remove?id=<?= $category->id ?>">Remove</a>
</p>

