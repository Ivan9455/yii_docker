<?php

use app\models\Category;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="category-edit">
    <div class="row">
        <div class="col-lg-8">
            <?php
            if (Yii::$app->session->hasFlash('success')) {
                echo Yii::$app->session->getFlash('success');
            } elseif (Yii::$app->session->hasFlash('error')) {
                echo Yii::$app->session->getFlash('error');
            }
            $form = ActiveForm::begin(['id' => 'save-form', 'options' => ['enctype' => 'multipart/form-data']]);
            ?>
            <?= $form->field($model, 'id')->hiddenInput()->label(''); ?>
            <?= $form->field($model, 'parent_id')->label('parent category')->dropDownList(
                Category::getDropDownList(),
                $params ? $params : ['prompt' => 'выберете катигорию']); ?>

            <?= $form->field($model, 'title')->textInput(); ?>
            <?= $form->field($model, 'description')->textarea(['rows' => '3']); ?>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
