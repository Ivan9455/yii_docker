<?php

use yii\grid\GridView;
use app\models\Category;

?>
<p>
    <a class="btn btn-primary" href="/admin/category/create">Create category</a>
</p>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [
            'attribute' => 'parent_id',
            'label' => 'Родительская категория',
            'format' => 'text',
            'content' => function (Category $data) {
                return $data->getParent()->title;
            },
        ],
        'title:ntext',
        'description:ntext',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]);
?>
