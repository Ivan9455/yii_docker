<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\admin\models\form\PostForm */
/* @var $categories array of \app\models\Category */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\User;
$this->title = 'Update';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="post-create">
    <div class="row">
        <div class="col-lg-5">
            <?php
            if (Yii::$app->session->hasFlash('success')) {
                echo Yii::$app->session->getFlash('success');
            } elseif (Yii::$app->session->hasFlash('error')) {
                echo Yii::$app->session->getFlash('error');
            }
            $form = ActiveForm::begin(['id' => 'save-form']);
            ?>
            <?= $form->field($model, 'username')->input('text'); ?>
            <?= $form->field($model, 'email')->input('text'); ?>
            <?= $form->field($model, 'role')->input('text')->dropDownList(User::getRoleAll()); ?>
            <?= $form->field($model, 'status')->input('text')->dropDownList(User::getStatusAll()); ?>
            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
