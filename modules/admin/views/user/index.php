<?php

use yii\grid\GridView;
use \app\models\User;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'username:ntext',
        'email:ntext',
        [
            'attribute' => 'status',
            'label' => 'Статус',
            'format' => 'text',
            'content' => function (User $data) {
                return $data->getStatus();
            },
            'filter' => User::getStatusAll()
        ],
        [
            'attribute' => 'role',
            'label' => 'Роль',
            'format' => 'text',
            'content' => function (User $data) {
                return $data->getRole();
            },
            'filter' => User::getRoleAll()
        ],
        ['class' => 'yii\grid\ActionColumn'],
    ],
]);
?>
