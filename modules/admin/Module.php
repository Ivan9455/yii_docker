<?php

namespace app\modules\admin;

use Yii;
use app\models\Admin;
use yii\web\NotFoundHttpException;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (Yii::$app->request->getPathInfo() != 'admin/login') {
            if (Yii::$app->user->identity->role != Admin::ROLE) {
                throw new NotFoundHttpException(Yii::t('yii', 'not permission '));
            } else {
                Yii::$app->layout = 'admin/main';
            }
        }
    }
}
