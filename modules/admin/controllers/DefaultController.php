<?php

namespace app\modules\admin\controllers;

use app\models\Admin;
use Yii;
use yii\web\Controller;
use app\models\form\admin\LoginForm;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (Yii::$app->user->identity->role == Admin::ROLE) {
            return $this->redirect('/admin/index');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin/index');
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }
}
