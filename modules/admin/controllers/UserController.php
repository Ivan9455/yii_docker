<?php

namespace app\modules\admin\controllers;

use app\models\Admin;
use Yii;
use yii\web\Controller;
use app\modules\admin\models\search\UserSearch;
use app\models\User;
use app\modules\admin\models\form\UserForm;

class UserController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->setPagination(['pageSize' => Yii::$app->params['admin']['user']['pageSize']]);

        return $this->render('index',
            ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionView(int $id)
    {
        return $this->render('view', ['user' => User::findOne($id)]);
    }

    public function actionUpdate(int $id)
    {
        if ($user = User::findOne($id)) {
            if($user->role == Admin::ROLE){
                return $this->render('error', ['message' => 'cannot edit admin']);
            }
            $model = new UserForm();
            $model->attributes = $user->getAttributes();
            if ($model->load(Yii::$app->request->post())) {
                $model->update($user);
            }

            return $this->render('update', [
                'model' => $model
            ]);
        }

        return $this->render('error', ['message' => 'user not found']);
    }

    public function actionDelete(int $id)
    {
        $user = User::findOne($id);
        if($user->role == Admin::ROLE){
            return $this->render('error', ['message' => 'cannot edit admin']);
        }elseif (is_null($user)) {
            return $this->render('delete', ['message' => 'Данный пользователь не был удален так как не найден']);
        }
        $user->delete();
        return $this->render('delete', ['message' => 'Пост успешно удален']);
    }

}
