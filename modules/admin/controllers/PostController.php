<?php

namespace app\modules\admin\controllers;

use app\models\Post;
use Yii;
use app\modules\admin\models\search\PostSearch;
use yii\web\Controller;
use app\modules\admin\models\form\PostForm;

class PostController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->setPagination(['pageSize' => Yii::$app->params['admin']['post']['pageSize']]);

        return $this->render('index',
            ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'message' => 'Не проверенные посты']);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionView(int $id)
    {
        if ($post = Post::findOne($id)) {
            return $this->render('view', ['post' => $post]);
        }

        return $this->render('/error', ['message' => 'post not found']);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate(int $id)
    {
        if ($post = Post::findOne($id)) {
            $model = new PostForm();
            $model->attributes = $post->getAttributes();
            if ($model->load(Yii::$app->request->post())) {
                $model->update($post);
            }

            return $this->render('edit', [
                'model' => $model
            ]);
        }

        return $this->render('/error', ['message' => 'post not found']);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete(int $id)
    {
        $post = Post::findOne($id);
        if (is_null($post)) {
            return $this->render('delete', ['message' => 'Данный пост не был удален так как не найден']);
        }
        $post->delete();
        return $this->render('delete', ['message' => 'Пост успешно удален']);
    }
}
