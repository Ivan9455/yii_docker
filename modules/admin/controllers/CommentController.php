<?php

namespace app\modules\admin\controllers;

use app\models\Comment;
use Yii;
use yii\web\Controller;
use app\modules\admin\models\search\CommentSearch;
use app\modules\admin\models\form\CommentForm;

class CommentController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->setPagination(['pageSize' => Yii::$app->params['admin']['comment']['pageSize']]);

        return $this->render('index',
            ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'message' => 'Не проверенные посты']);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionView(int $id)
    {
        if ($comment = Comment::findOne($id)) {
            return $this->render('view', ['comment' => $comment]);
        }

        return $this->render('/error', ['message' => 'comment not found']);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionUpdate(int $id)
    {
        if ($comment = Comment::findOne($id)) {
            $model = new CommentForm();
            $model->attributes = $comment->getAttributes();
            if ($model->load(Yii::$app->request->post())) {
                $model->update($comment);
            }

            return $this->render('update', [
                'model' => $model
            ]);
        }

        return $this->render('/error', ['message' => 'comment not found']);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete(int $id)
    {
        $comment = Comment::findOne($id);
        if (is_null($comment)) {
            return $this->render('delete', ['message' => 'Данный коментарий не был удален так как не найден']);
        }
        $comment->delete();
        return $this->render('delete', ['message' => 'Коментарий успешно удален']);
    }
}
