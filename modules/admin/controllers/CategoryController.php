<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use app\modules\admin\models\search\CategorySearch;
use Yii;
use yii\web\Controller;
use app\modules\admin\models\form\CategoryForm;

class CategoryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->setPagination(['pageSize' => Yii::$app->params['admin']['category']['pageSize']]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionView(int $id)
    {
        if ($category = Category::findOne($id)) {
            return $this->render('view', ['category' => $category]);
        }

        return $this->render('/error', ['message' => 'category not found']);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate(int $id)
    {
        if ($category = Category::findOne($id)) {
            $model = new CategoryForm();
            $model->setAttributes($category->getAttributes(), false);
            if ($model->load(Yii::$app->request->post())) {
                $model->update($category);
            }

            return $this->render('edit', ['model' => $model]);
        }

        return $this->render('/error', ['message' => 'category not found']);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new CategoryForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();

            return $this->redirect('/admin/categories');
        }

        return $this->render('edit', ['model' => $model]);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete(int $id)
    {
        $category = Category::findOne($id);
        if (is_null($category)) {
            return $this->render('delete', ['message' => 'Данный пост не был удален так как не найден']);
        }
        $category->delete();
        return $this->render('delete', ['message' => 'Пост успешно удален']);
    }
}
