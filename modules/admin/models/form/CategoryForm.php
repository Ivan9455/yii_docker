<?php

namespace app\modules\admin\models\form;

use Yii;
use yii\base\Model;
use app\models\Category;
use \yii\helpers\Html;

class CategoryForm extends Model
{
    public $id;
    public $parent_id;
    public $title;
    public $description;

    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            ['title', 'string', 'max' => 50],
            ['description', 'string', 'max' => 255],
            ['parent_id', 'checkParent'],
        ];
    }

    public function checkParent()
    {
        if ($this->parent_id !== null) {
            if (is_null(Category::findOne(['id' => $this->parent_id]))) {
                Yii::$app->session->setFlash('error', 'данной категории не существует');
            }
        }
    }

    /**
     * @param Category $category
     *
     * @return false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function update(Category $category)
    {
        $category->title = Html::encode($this->title);
        $category->description = Html::encode($this->description);

        return $category->update();
    }

    /**
     * @return bool
     */
    public function save():bool
    {
        $category = new Category();
        $category->setAttributes($this->getAttributes(), false);
        $category->title = Html::encode($this->title);
        $category->description = Html::encode($this->description);
        if($category->parent_id ===''){
            $category->parent_id = null;
        }

        return $category->save();
    }
}
