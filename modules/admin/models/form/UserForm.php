<?php

namespace app\modules\admin\models\form;

use yii\base\Model;

class UserForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $role;
    public $status;

    public function rules()
    {
        return [
            [['id'],'integer'],
            [['username','email','role','status'],'string']
        ];
    }


}
