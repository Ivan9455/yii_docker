<?php

namespace app\modules\admin\models\form;

use app\models\form\PostForm as Form;
use app\models\Post;

class PostForm extends Form
{
    public $status_check;

    public function rules() : array
    {
        return array_merge(parent::rules(), [
            [['status_check'], 'string']
        ]);
    }

    public function update(Post $post) : ?Post
    {
        $post->status_check = $this->status_check;
        $post->time_checked = date("Y-m-d H:i:s");

        return parent::update($post);
    }

}
