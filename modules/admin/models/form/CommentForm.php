<?php

namespace app\modules\admin\models\form;

use app\models\Comment;
use app\models\form\CommentForm as Form;

class CommentForm extends Form
{
    public $status_check;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status_check'], 'string']
        ]);
    }

    public function update(Comment $comment)
    {
        //var_dump($this->getAttributes());die;
        $comment->status_check = $this->status_check;
        $comment->time_checked = date("Y-m-d H:i:s");
        $comment->update();
    }

}
