<?php

namespace app\modules\admin\models\search;
use app\models\Category;
use yii\data\ActiveDataProvider;

class CategorySearch extends Category
{
    public static function tableName()
    {
        return 'category';
    }

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['description','title','parent_id'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Category::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['id' => $this->id]);
        $query->andWhere(sprintf(
            "parent_id in (select category.id from category where category.title like '%s%%')",
            $this->parent_id));
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
