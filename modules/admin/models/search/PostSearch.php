<?php

namespace app\modules\admin\models\search;

use app\models\Post;
use yii\data\ActiveDataProvider;

class PostSearch extends Post
{
    public static function tableName()
    {
        return 'post';
    }

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'content', 'user_id', 'category_id', 'status_check'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = Post::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['id' => $this->id]);
        $query->andWhere(sprintf(
            "user_id in (select user.id from user where user.username like '%s%%')", $this->user_id));
        $query->andWhere(sprintf(
            "category_id in (select category.id from category where category.title like '%s%%')",
            $this->category_id));
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'content', $this->content]);
        $query->andFilterWhere(['status_check' => $this->status_check]);

        return $dataProvider;
    }

}
