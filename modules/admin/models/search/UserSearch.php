<?php

namespace app\modules\admin\models\search;

use app\models\User;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['id'], 'string'],
            [['username', 'email', 'role', 'status'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = User::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'role', $this->role]);
        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
