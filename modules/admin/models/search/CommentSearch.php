<?php

namespace app\modules\admin\models\search;

use app\models\Comment;
use yii\data\ActiveDataProvider;

class CommentSearch extends Comment
{
    public static function tableName()
    {
        return 'comment';
    }

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['user_id', 'content', 'status_check'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Comment::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['id' => $this->id]);
        $query->andWhere(sprintf(
            "user_id in (select user.id from user where user.username like '%s%%')",
            $this->user_id));
        $query->andFilterWhere(['like', 'content', $this->content]);
        $query->andFilterWhere(['like', 'status_check', $this->status_check]);

        return $dataProvider;
    }
}
