let Rating = {
    like:1,
    dislike:2,
    save: function (post_id,status) {
        return $.ajax({
            type: "POST",
            url: "/rating/save",
            data: {
                post_id:post_id,
                status:status,
                _csrf : yii.getCsrfToken()
            },
        }).done(function (result) {
            console.log(result)
        })
    },
    eventStatus: function () {
        $('.container').on('click', '.item', function () {
            Rating.evStatus($(this));
            if ($(this).hasClass('like')) {
                Rating.save($(this)[0].parentElement.dataset.id,Rating.like)
            } else if ($(this).hasClass('dislike')) {
                Rating.save($(this)[0].parentElement.dataset.id,Rating.dislike)
            } else if ($(this).hasClass('bookmark')) {
                Bookmark.save($(this)[0].parentElement.dataset.id);
            }

        });
    },
    evStatus: function (item) {
        if (Rating.itemStatus(item, 0) && Rating.itemStatus(item, 1) && !item.hasClass('bookmark')) {
            $(item[0].children.item(0)).removeClass('far').addClass('fas');
            $(item[0].children.item(1))[0].innerText++;
        } else if (Rating.itemStatus(item, 0) && item.hasClass('like')) {
            $(item[0].parentElement.children.item(1).children.item(0)).removeClass('fas').addClass('far');
            $(item[0].children.item(0)).removeClass('far').addClass('fas');
            $(item[0].children.item(1))[0].innerText++;
            $(item[0].parentElement.children.item(1).children.item(1))[0].innerText--;
        } else if (Rating.itemStatus(item, 1) && item.hasClass('like')) {
            $(item[0].children.item(1))[0].innerText--;
            $(item[0].children.item(0)).removeClass('fas').addClass('far');
        } else if (Rating.itemStatus(item, 0) && item.hasClass('dislike')) {
            $(item[0].children.item(1))[0].innerText--;
            $(item[0].children.item(0)).removeClass('fas').addClass('far');
        } else if (Rating.itemStatus(item, 1) && item.hasClass('dislike')) {
            $(item[0].parentElement.children.item(0).children.item(0)).removeClass('fas').addClass('far');
            $(item[0].children.item(0)).removeClass('far').addClass('fas');
            $(item[0].children.item(1))[0].innerText++;
            $(item[0].parentElement.children.item(0).children.item(1))[0].innerText--;
        } else if (item.hasClass('bookmark')) {
            if ($(item[0].children.item(0)).hasClass('far')) {
                $(item[0].children.item(0)).removeClass('far').addClass('fas');
            } else {
                $(item[0].children.item(0)).removeClass('fas').addClass('far');
            }
        }
    },
    itemStatus: function (item, index) {
        return $(item[0].parentElement.children.item(index).children.item(0)).hasClass('far');
    },
};
