let Bookmark = {
    save:function (post_id) {
        return $.ajax({
            type: "POST",
            url: "/bookmark/save",
            data: {
                post_id:post_id,
                _csrf : yii.getCsrfToken()
            },
        }).done(function (result) {
            console.log(result)
        })
    }
}