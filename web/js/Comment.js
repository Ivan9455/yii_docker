let Comment = {
    update: function (data) {
        return $.ajax({
            type: "POST",
            url: "/comment/update",
            data: data
        })
    },
    edit: function (id) {
        return $.ajax({
            type: "POST",
            url: "/comment/edit",
            data: {
                _csrf: yii.getCsrfToken(),
                id: id
            },
        })
    },
    remove: function (id) {
        return $.ajax({
            type: "POST",
            url: "/comment/remove",
            data: {
                _csrf: yii.getCsrfToken(),
                id: id
            }
        })
    },
    get: function (id) {
        return $.ajax({
            type: "POST",
            url: "/comment/get",
            data: {
                _csrf: yii.getCsrfToken(),
                id: id
            },
        })
    },
    event: function () {
        $(".wrap").on("click", ".remove_comment", function (event) {
            event.preventDefault();
            let elem = $(this.parentElement.parentElement.parentElement)[0];
            Comment.remove($(this.parentElement)[0].dataset.id).then(function () {
                elem.innerHTML = "";
            })
        }).on("click", ".edit_comment", function (event) {
            event.preventDefault();
            let elem = $(this.parentElement.parentElement)[0];
            Comment.edit($(this.parentElement)[0].dataset.id).then(function (result) {
                elem.innerHTML = result;
            })
        }).on("click", ".get_comment", function (event) {
            event.preventDefault();
            let elem = $(this.parentElement.parentElement.parentElement.parentElement)[0];
            Comment.get($(this)[0].dataset.id).then(function (result) {
                elem.innerHTML = result;
            });
        }).on("click", ".comment_update", function (event) {
            event.preventDefault();
            let elem = $(this.parentElement.parentElement.parentElement.parentElement)[0];
            Comment.update($(this.parentElement.parentElement).serialize()).then(function (result) {

                if(result != false){
                    elem.innerHTML = result;
                }else {
                    console.log($(elem + " > .field-commentform-content"));
                }
            })
        });
    }
};
